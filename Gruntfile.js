module.exports = function(grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

		watch: {
			options:{
				livereload: true,
			},
			scripts: {
				files: ['app/js/animations.js'],
				tasks: ['uglify']
			},
			css: {
				files: [
                        'app/css/*.styl' 
                        ],
				tasks: ['stylus'],
				options: {
					livereload: true
				}
			},
			html: {
				files: ['app/*.jade'],
				tasks: ['jade'],
				options: {
					livereload: true,
					spawn: false
				}
			}
		},

		uglify : {
			build: {
				src: [
					'app/js/animations.js' 
				],
				dest: 'public/js/project.min.js' 
			}
		},

		stylus: {
			options: {
				compress: true
			},
			compile: {
				files: {
					'public/css/project.min.css' : 'app/css/index.styl'
				}
			}
		},

		jade: {
			compile: {
				options: {
					data: {
						debug: false
					}
				},
				files: {
					'index.html' : 'app/index.jade'
				}
			}
		},

		connect: {
            server: {
                options: {
                    hostname: '*',
                    port: 8800
                }
            }
        }

    });

    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-stylus');
    grunt.loadNpmTasks('grunt-contrib-jade');
    grunt.loadNpmTasks('grunt-contrib-connect');

    grunt.registerTask('default', ['connect', 'uglify', 'stylus', 'jade', 'watch']);

};
