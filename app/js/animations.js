$(function(){
	// ANIMATING CSS PROPERTIES

	// TweenMax.to(target, duration, {vars});
	// TweenMax.to($(".logo"), 2, {left:600});
	//TweenMax.to([$(".title"), $(".logo")], 2, {left:600});


	// css plugin will default to px
	// pass in units in quotation
	//TweenMax.to(".logo", 2, {left:600, backgroundColor:"#f00", padding:100, borderRadius:"20%"});


	// styles are being updated during the duration of the tween

	// CSS plugin does everything in the background

	// ANIMATING CSS TRANSFORMS

	//TweenMax.to(".logo", 2, {x:600, rotation:360, scale:0.5});



	// ANIMATION SPEEDS Rate of change // ease

	// TweenMax.to(".logo", 2, {x:600, ease:Back.easeOut});
	//TweenMax.to(".logo", 2, {x:600, ease:Bounce.easeOut});

	//go to :http://greensock.com/ease-visualizer


	// On load you want  from to 
	//TweenMax.from(".logo", 3, {opacity:0, scale:0, ease:Bounce.easeOut});


	// Tweening title
	//TweenMax.from(".title > span", 2, {opacity:0, y:200});


	// Tweening Title and logo
	//TweenMax.from(".title > span", 0.5, {opacity:0, y:200});
	//TweenMax.from(".logo", 0.5, {opacity:0, scale:0, delay: 0.5, ease:Bounce.easeOut});

	// Delay
	//TweenMax.from(".logo", 3, {opacity:0, scale:0, ease:Bounce.easeOut});
	//TweenMax.staggerFrom(".title > span", 0.5, {opacity:0, y:200, rotation:360, scale:2, delay:4});

	// Stagger letters
	//TweenMax.from(".logo", 3, {opacity:0, scale:0, ease:Bounce.easeOut});
	//TweenMax.staggerFrom(".title > span", 0.5, {opacity:0, y:200, rotation:360, scale:2, delay:4}, 0.2);


	//Hide everything after
	/*
	TweenMax.from(".logo", 3, {opacity:0, scale:0, ease:Bounce.easeOut});
	TweenMax.staggerFrom(".title > span", 0.5, {opacity:0, y:200, rotation:360, scale:2, delay:2}, 0.2);
	TweenMax.to(".logo, .title > span", 0.5, {opacity:0, delay:10, onComplete:complete});


	function complete() {
		alert('complete');
	}
	*/

	var tl = new TimelineMax();

	tl.from(".logo", 3, {opacity:0, scale:0, ease:Bounce.easeOut});
	tl.staggerFrom(".title > span", 0.5, {opacity:0, y:200, rotation:360, scale:2, delay:2}, 0.2);
	tl.to(".logo, .title > span", 0.5, {opacity:0, delay:10, onComplete:complete});
	
	function complete() {
		alert('complete');
	}

});
